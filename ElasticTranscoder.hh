<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\media\elasticTranscoder
{
	use nuclio\core\ClassManager;
	use nuclio\core\plugin\Plugin;
	use \Aws\ElasticTranscoder\ElasticTranscoderClient;
	use \Aws\Common\Aws;
	use nuclio\plugin\workflow\model\StepTransition;
	use nuclio\plugin\workflow\model\StepTransitionAction;
	use nuclio\plugin\workflow\model\Action;
	use nuclio\plugin\media\elasticTranscoder\ElasticTranscoderException;

	class ElasticTranscoder extends Plugin
	{
		private ElasticTranscoderClient $client;
		private string $key;
		private string $secret;
		private string $region;

		public static function getInstance(...$args):ElasticTranscoder
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}

		public function __construct(string $key, string $secret, string $region, ?string $proxy = null):void
		{
			$this->key=$key;
			$this->secret=$secret;
			$this->region=$region;
			$config = 
			[
				'key' => $key,
				'secret' => $secret,
				'region' => $region,
			];
			if(!is_null($proxy))
			{
				$config['request.options'] = ['proxy' => $proxy];
			}
			$this->client=ElasticTranscoderClient::factory($config);
			parent::__construct();
		}
		
		public function createPipeLine(string $pipelineName, string $inputBucket, string $outputBucket, string $roleArn, ?string $notificationArn=null):mixed
		{
			if(!is_null($notificationArn))
			{
				$notification = array_fill_keys(['Progressing', 'Completed', 'Warning', 'Error'], $notificationArn);
			}
			else
			{
				$notification = array_fill_keys(['Progressing', 'Completed', 'Warning', 'Error'], '');	
			}
			$result=$this->client->createPipeline
			( 
				[
					'Name'=>		$pipelineName,
					'InputBucket'=>	$inputBucket,
					'OutputBucket'=>$outputBucket,
					'Role'=>		$roleArn,
					'Notification'=> $notification,
				]
			);
			return $result['Pipeline']['Id']; 
		}

		public function deletePipeline(string $id):mixed
		{
			$result=$this->client->deletePipeline
			( 
				[
					'Id'=>$id
				]
			);
			return $result; 	
		}

		public function createJob(string $piplineId, string $inputFileName, string $outPutfileName, string $presetId, ?string $outputKeyPrefix=null, ?string $thumbnailPattern=null):mixed
		{
			$params = 
			[
				'PipelineId' => $piplineId,
				'Input' => 
				[
					'Key' => $inputFileName,
					'FrameRate' => 'auto',
					'Resolution' => 'auto',
					'AspectRatio' => 'auto',
					'Interlaced' => 'auto',
					'Container' => 'auto',
				],
				'Outputs' => 
				[
					[
						'Key' => $outPutfileName,
						'Rotate' => 'auto',
						'PresetId' => $presetId,
					],
				],
			];
			if(!is_null($outputKeyPrefix))
			{
				$params['OutputKeyPrefix'] = $outputKeyPrefix;
			}
			if(!is_null($thumbnailPattern))
			{
				$params['Outputs'][0]['ThumbnailPattern'] = $thumbnailPattern;
			}
			$job=$this->client->createJob($params);
			return $job;
		}

		public function cancelJob(string $jobId):mixed
		{
			$result=$this->client->cancelJob
			(
				array
				(
					'Id'=> 'string',
				)
			);
			return $result;
		}

		public function jobStatus(string $jobId):mixed
		{
			$response=$this->client->readJob(array('Id'=>$jobId));
			$jobData=$response->get('Job');
			return $jobData;
		}

		public function listJobsByPipeline(string $piplineId, ?string $ascending=null, ?string $pageToken=null):mixed
		{
			$result=$this->client->listJobsByPipeline
			(
				array
				(
					    // PipelineId is required
					    'PipelineId' => $piplineId,
					    'Ascending' => $ascending,
					    'PageToken' => $pageToken,
				)
			);
			return $result;
		}

		public function listPipelines(?string $ascending=null, ?string $pageToken=null)
		{
			$result=$this->client->listPipelines
			(
				array
				(
					'Ascending'	=> $ascending,
					'PageToken' => $pageToken
				)
			);
		}

		public function readPreset(string $id):mixed
		{
			$result=$this->client->readPreset
			(
				[
					'Id' => $id
				]
			);
			return $result;
		}

		public function transcodeWorkflow(string $pipelineId, Map<string,string> $params):bool
		{
			if(!$params->containsKey('inputFileName') || is_null($inputFileName = $params->get('inputFileName')))
			{
				throw new ElasticTranscoderException("No input file given");
			}
			if(!$params->containsKey('outputFileName') || is_null($outputFileName = $params->get('outputFileName')))
			{
				throw new ElasticTranscoderException("No output file given");
			}
			if(!$params->containsKey('presetId') || is_null($presetId = $params->get('presetId')))
			{
				throw new ElasticTranscoderException("No preset Id file given");
			}
			$params->containsKey('outputKeyPrefix')?$outputKeyPrefix=$params->get('outputKeyPrefix'):$outputKeyPrefix=null;
			$params->containsKey('thumbnailPattern')?$thumbnailPattern=$params->get('thumbnailPattern'):$thumbnailPattern=null;
			$job = $this->createJob($pipelineId, $inputFileName, $outputFileName, $presetId, $outputKeyPrefix, $thumbnailPattern);
			return true;
		}
	}
}
